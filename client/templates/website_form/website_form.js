Template.website_form.events({
  'submit .js-save-website-form': function(event) {
    // here is an example of how to get the url out of the form:
    var url,
        title,
        description;

    if (Meteor.user()) {
      url = event.target.url.value;
      title = event.target.title.value;
      description = event.target.description.value;

      Meteor.call("getDescription", url, function(error, results) {
        console.log(error); //results.data should be a JSON object
        console.log(results); //results.data should be a JSON object
        console.log($.parseHTML( results ));
      });

      if (url && title && description) {
        Websites.insert({
          title: title,
          url: url,
          description: description,
          createdOn: new Date(),
          createdBy: Meteor.user()._id
        });
      };
    }

    $(event.target).trigger('reset');
    $(event.target).closest('.modal').modal('hide');

    return false;
  }
});