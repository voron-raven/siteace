Template.navbar.helpers({
  activeIfTemplateIs: function (path) {
    return path === Router.current().route.getName() ? 'active' : '';
  },
  username: function() {
    return Meteor.user() ? Meteor.user().username : "anonymous";
  }
});
